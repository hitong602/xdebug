cmake_minimum_required(VERSION 3.10 FATAL_ERROR)
project(xdebug)

set(PROJECT_VERSION  "0.1.0")
set(PROJECT_VERSION_major  "0")

find_package(PkgConfig REQUIRED)
pkg_search_module(XDEBUG REQUIRED  xcb-debug IMPORTED_TARGET)
pkg_search_module(XCB REQUIRED  xcb IMPORTED_TARGET)

aux_source_directory(./ DIR_SRCS)

include_directories(${XDEBUG_INCLUDE_DIRS})
message(${XDEBUG_LIBRARIES})


add_executable(xdebug ${DIR_SRCS})
target_link_libraries(xdebug PkgConfig::XCB)
target_link_libraries(xdebug PkgConfig::XDEBUG)

