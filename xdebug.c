#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>
#include <xcb/debug.h>

#include <X11/Xlibint.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

/* for this struct, refer to libxnee */
typedef union
{
	unsigned char type;
	xEvent event;
	xResourceReq req;
	xGenericReply reply;
	xError error;
	xConnSetupPrefix setup;
} XRecordDatum;

xcb_connection_t *ctrl_dpy;
xcb_connection_t *data_dpy;
const char *ProgramName;
static  int lastEventType = 0;
static  int repeatEventCount = 0; //重复的事件数
int stop = 0;


struct _extinfo {
	int baseid;
	const char * name;
};

struct _extinfo  extinfo []= {
	{128,"Generic Event Extension"},
	{129,"SHAPE"},
	{130,"MIT-SHM"},
	{131,"XInputExtension"},
	{132,"XTEST"},
	{133,"BIG-REQUESTS"},
	{134,"SYNC"},
	{135,"XKEYBOARD"},
	{136,"XC-MISC"},
	{137,"XFIXES"},
	{138,"RENDER"},
	{139,"RANDR"},
	{140,"XINERAMA"},
	{141,"COMPOSITE"},
	{142,"DAMAGE"},
	{143,"MIT-SCREEN-SAVER"},
	{144,"DOUBLE-BUFFER"},
	{145,"RECORD"},
	{146,"DEBUG"},
	{147,"Present"},
	{148,"X-Resource"},
	{149,"XVideo"},
	{150,"GLX"},
};

static void usage(const char *errmsg)
{
}

const char *xdebug_translate_error(int errcode)
{
	switch (errcode)
	{
	case 1:
		return "BadRequest";
	case 2:
		return "BadValue";
	case 3:
		return "BadWindow";
	case 4:
		return "BadPixmap";
	case 5:
		return "BadAtom";
	case 6:
		return "BadCursor";
	case 7:
		return "BadFont";
	case 8:
		return "BadMatch";
	case 9:
		return "BadDrawable";
	case 10:
		return "BadAccess";
	case 11:
		return "BadAlloc";
	case 12:
		return "BadColormap";
	case 13:
		return "BadGContext";
	case 14:
		return "BadIDChoice";
	case 15:
		return "BadName";
	case 16:
		return "BadLength";
	case 17:
		return "BadImplementation";
		//   case EXT_MITSHM | 0:
		//     return "BadShmSeg";
		//   case EXT_RENDER | 0:
		//     return "BadPictFormat";
		//   case EXT_RENDER | 1:
		//     return "BadPicture";
		//   case EXT_RENDER | 2:
		//     return "BadPictOp";
		//   case EXT_RENDER | 3:
		//     return "BadGlyphSet";
		//   case EXT_RENDER | 4:
		//     return "BadGlyph";
	default:
		return NULL;
	}
}
//解析
const char *xdebug_translate_event(int eventtype)
{
	switch (eventtype & ~0x80)
	{
	case 2:
		return "KeyPress";
	case 3:
		return "KeyRelease";
	case 4:
		return "ButtonPress";
	case 5:
		return "ButtonRelease";
	case 6:
		return "MotionNotify";
	case 7:
		return "EnterNotify";
	case 8:
		return "LeaveNotify";
	case 9:
		return "FocusIn";
	case 10:
		return "FocusOut";
	case 11:
		return "KeymapNotify";
	case 12:
		return "Expose";
	case 13:
		return "GraphicsExposure";
	case 14:
		return "NoExposure";
	case 15:
		return "VisibilityNotify";
	case 16:
		return "CreateNotify";
	case 17:
		return "DestroyNotify";
	case 18:
		return "UnmapNotify";
	case 19:
		return "MapNotify";
	case 20:
		return "MapRequest";
	case 21:
		return "ReparentNotify";
	case 22:
		return "ConfigureNotify";
	case 23:
		return "ConfigureRequest";
	case 24:
		return "GravityNotify";
	case 25:
		return "ResizeRequest";
	case 26:
		return "CirculateNotify";
	case 27:
		return "CirculateRequest";
	case 28:
		return "PropertyNotify";
	case 29:
		return "SelectionClear";
	case 30:
		return "SelectionRequest";
	case 31:
		return "SelectionNotify";
	case 32:
		return "ColormapNotify";
	case 33:
		return "ClientMessage";
	case 34:
		return "MappingNotify";
		//   case EXT_MITSHM | 0:
		//     return "ShmCompletion";
	default:
		return NULL;
	}
}

const char *xdebug_translate_request(int opcode)
{
	switch (opcode)
	{
	case 0:
		break;
	case 1:
		return "X_CreateWindow";
	case 2:
		return "X_ChangeWindowAttributes";
	case 3:
		return "X_GetWindowAttributes";
	case 4:
		return "X_DestroyWindow";
	case 5:
		return "X_DestroySubwindows";
	case 6:
		return "X_ChangeSaveSet";
	case 7:
		return "X_ReparentWindow";
	case 8:
		return "X_MapWindow";
	case 9:
		return "X_MapSubwindows";
	case 10:
		return "X_UnmapWindow";
	case 11:
		return "X_UnmapSubwindows";
	case 12:
		return "X_ConfigureWindow";
	case 13:
		return "X_CirculateWindow";
	case 14:
		return "X_GetGeometry";
	case 15:
		return "X_QueryTree";
	case 16:
		return "X_InternAtom";
	case 17:
		return "X_GetAtomName";
	case 18:
		return "X_ChangeProperty";
	case 19:
		return "X_DeleteProperty";
	case 20:
		return "X_GetProperty";
	case 21:
		return "X_ListProperties";
	case 22:
		return "X_SetSelectionOwner";
	case 23:
		return "X_GetSelectionOwner";
	case 24:
		return "X_ConvertSelection";
	case 25:
		return "X_SendEvent";
	case 26:
		return "X_GrabPointer";
	case 27:
		return "X_UngrabPointer";
	case 28:
		return "X_GrabButton";
	case 29:
		return "X_UngrabButton";
	case 30:
		return "X_ChangeActivePointerGrab";
	case 31:
		return "X_GrabKeyboard";
	case 32:
		return "X_UngrabKeyboard";
	case 33:
		return "X_GrabKey";
	case 34:
		return "X_UngrabKey";
	case 35:
		return "X_AllowEvents";
	case 36:
		return "X_GrabServer";
	case 37:
		return "X_UngrabServer";
	case 38:
		return "X_QueryPointer";
	case 39:
		return "X_GetMotionEvents";
	case 40:
		return "X_TranslateCoords";
	case 41:
		return "X_WarpPointer";
	case 42:
		return "X_SetInputFocus";
	case 43:
		return "X_GetInputFocus";
	case 44:
		return "X_QueryKeymap";
	case 45:
		return "X_OpenFont";
	case 46:
		return "X_CloseFont";
	case 47:
		return "X_QueryFont";
	case 48:
		return "X_QueryTextExtents";
	case 49:
		return "X_ListFonts";
	case 50:
		return "X_ListFontsWithInfo";
	case 51:
		return "X_SetFontPath";
	case 52:
		return "X_GetFontPath";
	case 53:
		return "X_CreatePixmap";
	case 54:
		return "X_FreePixmap";
	case 55:
		return "X_CreateGC";
	case 56:
		return "X_ChangeGC";
	case 57:
		return "X_CopyGC";
	case 58:
		return "X_SetDashes";
	case 59:
		return "X_SetClipRectangles";
	case 60:
		return "X_FreeGC";
	case 61:
		return "X_ClearArea";
	case 62:
		return "X_CopyArea";
	case 63:
		return "X_CopyPlane";
	case 64:
		return "X_PolyPoint";
	case 65:
		return "X_PolyLine";
	case 66:
		return "X_PolySegment";
	case 67:
		return "X_PolyRectangle";
	case 68:
		return "X_PolyArc";
	case 69:
		return "X_FillPoly";
	case 70:
		return "X_PolyFillRectangle";
	case 71:
		return "X_PolyFillArc";
	case 72:
		return "X_PutImage";
	case 73:
		return "X_GetImage";
	case 74:
		return "X_PolyText8";
	case 75:
		return "X_PolyText16";
	case 76:
		return "X_ImageText8";
	case 77:
		return "X_ImageText16";
	case 78:
		return "X_CreateColormap";
	case 79:
		return "X_FreeColormap";
	case 80:
		return "X_CopyColormapAndFree";
	case 81:
		return "X_InstallColormap";
	case 82:
		return "X_UninstallColormap";
	case 83:
		return "X_ListInstalledColormaps";
	case 84:
		return "X_AllocColor";
	case 85:
		return "X_AllocNamedColor";
	case 86:
		return "X_AllocColorCells";
	case 87:
		return "X_AllocColorPlanes";
	case 88:
		return "X_FreeColors";
	case 89:
		return "X_StoreColors";
	case 90:
		return "X_StoreNamedColor";
	case 91:
		return "X_QueryColors";
	case 92:
		return "X_LookupColor";
	case 93:
		return "X_CreateCursor";
	case 94:
		return "X_CreateGlyphCursor";
	case 95:
		return "X_FreeCursor";
	case 96:
		return "X_RecolorCursor";
	case 97:
		return "X_QueryBestSize";
	case 98:
		return "X_QueryExtension";
	case 99:
		return "X_ListExtensions";
	case 100:
		return "X_ChangeKeyboardMapping";
	case 101:
		return "X_GetKeyboardMapping";
	case 102:
		return "X_ChangeKeyboardControl";
	case 103:
		return "X_GetKeyboardControl";
	case 104:
		return "X_Bell";
	case 105:
		return "X_ChangePointerControl";
	case 106:
		return "X_GetPointerControl";
	case 107:
		return "X_SetScreenSaver";
	case 108:
		return "X_GetScreenSaver";
	case 109:
		return "X_ChangeHosts";
	case 110:
		return "X_ListHosts";
	case 111:
		return "X_SetAccessControl";
	case 112:
		return "X_SetCloseDownMode";
	case 113:
		return "X_KillClient";
	case 114:
		return "X_RotateProperties";
	case 115:
		return "X_ForceScreenSaver";
	case 116:
		return "X_SetPointerMapping";
	case 117:
		return "X_GetPointerMapping";
	case 118:
		return "X_SetModifierMapping";
	case 119:
		return "X_GetModifierMapping";
	case 127:
		return "X_NoOperation";
	case 0x8d00:
		return "Compositor_QueryVersion";
	case 0x8d01:
		return "Compositor_RedirectWindow";
	case 0x8d02:
		return "Compositor_RedirectSubWindows";
	case 0x8d03: 
		return "Compositor_UnredirectWindow";
	case 0x8d04:
		return "Compositor_UnresirectSubWindows";
	case 0x8d05:
		return "Compositor_CreateRegionFromBorderClip";
	case 0x8d06:
		return "Compositor_NameWindowPixmap";
	case 0x8d07:
		return "Compositor_GetOverlayWindow";
	case 0x8d08:
		return "Compositor_ReleaseOverlayWindow";
	case 0x8e00:
		return "X_DamageQueryVersion";
	case 0x8e01:
		return "X_DamageCreate";
	case 0x8e02:
		return "X_DamageDestroy";
	case 0x8e03:
		return "X_DamageSubtract";
	case 0x8e04:
		return "X_DamageAdd";
	default:
		break;
	}
	static char s[50];

	int extbaseid=(opcode >>8) & 0xFF;
	int extfuncid=opcode & 0xFF;

	if(extbaseid >=128 && extbaseid <=150) {
		sprintf(s, "ext request : %s-%d", extinfo[extbaseid-128].name,extfuncid);
	} else {
		s[0]='\0';
	}
	//sprintf(s, "can not parse opcode : %d-%d", (opcode >>8) & 0xFF, opcode & 0xFF);
	return (const char *)&s[0];
}

size_t xorg_data_callback(xcb_debug_probe_context_reply_t *reply, uint8_t *data_)
{
	/* FIXME: we need use XQueryPointer to get the first location */
	static int cur_x = 0;
	static int cur_y = 0;

	XRecordDatum *data = (XRecordDatum *)data_;

	int time = reply->server_time;

	if (reply->category == 0) //event reply error
	{
		int event_type = data->type;
		
		BYTE btncode, keycode;
		btncode = keycode = data->event.u.u.detail;

		int rootx = data->event.u.keyButtonPointer.rootX;
		int rooty = data->event.u.keyButtonPointer.rootY;

		const char *eventname = xdebug_translate_event(event_type);

		if( lastEventType == event_type && eventname) {
			repeatEventCount ++;
			printf("\r Event: %s received (%d)", eventname,repeatEventCount);

		} else {
			if(repeatEventCount> 0 ) 
				printf("\n");

			lastEventType = event_type;
			repeatEventCount = 0;

			if (eventname)
			{
				printf(" Event: %s received \n", eventname);
			}

			if (data_[0] == 0)
				return ((*(uint32_t *)&data_[4]) + 8) << 2;
		}
		return 32;

	}
	else if (reply->category == 1)  //request
	{
		
		int opcode = data->req.reqType; //core request
		if(opcode > 127 ) {
			// 获得扩展的id ,和其函数的id
			int majorop = data_[0];
  			int minorop = data_[1];

			//将这两个id合成一个１６位数据
			 opcode = ((majorop << 8) + minorop) & 0xffff;
		}

		const char *requestname = xdebug_translate_request(opcode);
		if (requestname) {
			if(repeatEventCount> 0 ) {
				printf("\n");
				repeatEventCount =0;
			}
				
			printf("client send request: %s \n", requestname);
		}
			

		return data->req.length <<2 ;
	}

}

int main(int argc, char **argv)
{
	//int screen_num;
	xcb_screen_t *root;

	int majorVer;
	int minorVer;

	uint32_t mask = 0;

	int i = 0;
	int w = 0;
	ProgramName = argv[0];

	for (i = 1; i < argc; i++)
	{
		char *arg = argv[i];

		if (arg[0] == '-')
		{
			switch (arg[1])
			{
			case 'i': /* -id */
				if (++i >= argc)
					usage("-id requires an argument");
				sscanf(argv[i], "0x%lx", &w);
				if (!w)
					sscanf(argv[i], "%lu", &w);
				if (!w)
				{
					fprintf(stderr,
							"%s: unable to parse argument '%s' for -id\n",
							ProgramName, argv[i]);
					usage(NULL);
				}
				continue;
			default:
				goto unrecognized;
			} /* end switch on - */
		}
		else
		{
		unrecognized:
			fprintf(stderr, "%s: unrecognized argument '%s'\n",
					ProgramName, arg);
			usage(NULL);
			exit(1);
		}
	}

	ctrl_dpy = xcb_connect(0, NULL);
	data_dpy = xcb_connect(0, NULL);
	//root = xcb_aux_get_screen(c, screen_num);

	if (w == 0)
	{
		fprintf(stderr, "id requires an argument \n");
		//以后　可以模拟xev 创建个窗口，获取窗口的ｉｄ
		exit(1);
	}

	if (xcb_connection_has_error(ctrl_dpy) || xcb_connection_has_error(data_dpy))
	{
		printf("Connect failed.\n");
		exit(1);
	}

	xcb_debug_query_version_cookie_t xuqvc = xcb_debug_query_version(ctrl_dpy, 0, 0);
	xcb_debug_query_version_reply_t *xuqvr = xcb_debug_query_version_reply(ctrl_dpy, xuqvc, 0);
	if (!xuqvr)
	{
		printf("debug get version error!\n");
		xcb_disconnect(ctrl_dpy);
		return 1;
	}
	printf("debug is OK! version %d.%d \n", xuqvr->major_version, xuqvr->minor_version);
	free(xuqvr);

	xcb_debug_context_t uc = xcb_generate_id(ctrl_dpy);

	xcb_void_cookie_t create_cookie = xcb_debug_create_context_checked(ctrl_dpy, uc);
	xcb_generic_error_t *error = xcb_request_check(ctrl_dpy, create_cookie);
	if (error)
	{
		fprintf(stderr, "Cound  not create log context");
		free(error);
		exit(4);
	}

	xcb_void_cookie_t register_cookie = xcb_debug_register_client_checked(ctrl_dpy, uc, w);
	error = xcb_request_check(ctrl_dpy, register_cookie);
	if (error)
	{
		fprintf(stderr, "Cound  not create log context");
		free(error);
		exit(4);
	}

	printf("debug register window: %d OK! \n", w);

	xcb_debug_probe_context_cookie_t xupcc = xcb_debug_probe_context(data_dpy, uc);

	while (!stop)
	{
		xcb_debug_probe_context_reply_t *reply = xcb_debug_probe_context_reply(data_dpy, xupcc, NULL);
		if (!reply)
		{
			continue;
		}
		//printf("data received : %d OK! \n", w);

		uint8_t *data = xcb_debug_probe_context_data(reply);
		size_t offset = 0;
		while (offset < reply->length << 2)
		{
			offset += xorg_data_callback(reply, &data[offset]);
		}
		free(reply);
	}

	xcb_debug_unregister_client(ctrl_dpy, uc, w);
	xcb_debug_free_context(ctrl_dpy, uc);
	xcb_flush(ctrl_dpy);

	xcb_disconnect(data_dpy);
	xcb_disconnect(ctrl_dpy);

	return 0;
}
