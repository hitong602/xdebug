#!/bin/bash


CLOCKPID=`ps -ef |grep roll |grep -v grep | awk '{print $2}'`
if [ "$CLOCKPID" != "" ]; then
    kill -9 $CLOCKPID
fi

OPENBOXPID=`ps -ef |grep openbox |grep -v grep | awk '{print $2}'`
if [ "$OPENBOXPID" != "" ]; then
    kill -9 $OPENBOXPID
fi



DISPLAY=:1 ../rollxcb/build/rollxcb &
DISPLAY=:1 openbox &

sleep 3
pid=$(pidof rollxcb)

echo $pid

DISPLAY=:1 ./_build/xdebug  -id $pid
