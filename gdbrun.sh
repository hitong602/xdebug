#!/bin/bash


CLOCKPID=`ps -ef |grep xclock |grep -v grep | awk '{print $2}'`
if [ "$CLOCKPID" != "" ]; then
    kill -9 $CLOCKPID
fi

OPENBOXPID=`ps -ef |grep openbox |grep -v grep | awk '{print $2}'`
if [ "$OPENBOXPID" != "" ]; then
    kill -9 $OPENBOXPID
fi



xclock &
openbox &

sleep 3
pid=$(pidof xclock)


gdb --args ./_build/xdebug  -id $pid
